import React from 'react'
import { connect } from 'react-redux'

export function FilterForm({ filter, setFilter }) {
  return (
    <div>
      <input min="0"
        className="form-control"
        value={filter}
        placeholder="Поиск"
        type="number"
        onChange={({ target }) => setFilter(Number(target.value))} />
    </div>
  )
}

FilterForm.propTypes = {
  filter: React.PropTypes.number,
  setFilter: React.PropTypes.func
}

function stateToProps(state) {
  return {
    filter: state.filter
  }
}
function dispatchToProps(dispatch) {
  return {
    setFilter(newFilters) {
      dispatch(
        {
          type: 'SET_FILTER',
          payload: newFilters
        }
      )
    }
  }
}

export default connect(stateToProps, dispatchToProps)(FilterForm)
