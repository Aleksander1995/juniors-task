const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
    app: [ './src/index.js' ]
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/assets/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({ DATA_HOST: '"192.168.88.66:8080"' })
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: [ 'react', 'es2015', 'stage-2' ]
        }
      },
      {
        test: /\.s?css$/,
        loaders: [ 'style', 'css', 'sass' ]
      },
      {
        test: /\.(eot|woff2?|ttf|svg)(\?v.*)?$/,
        loader: 'file-loader?name=gen/font-[sha512:Hash:base64:9].[ext]'
      }
    ]
  }
}
